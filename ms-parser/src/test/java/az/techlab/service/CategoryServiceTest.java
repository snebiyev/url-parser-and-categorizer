package az.techlab.service;

import az.techlab.exception.InvalidUrlException;
import az.techlab.helper.CategoryHelper;
import az.techlab.model.Category;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CategoryServiceTest {
    CategoryHelper categoryHelper = new CategoryHelper();
    CategoryService categoryService = new CategoryService(categoryHelper);

    @Test
    @DisplayName("Test for null subset given of categories")
    void findMatchedCategoriesGivenNullSubsetShouldReturnMinimumOneCategory() {
        var responseDto = categoryService.findMatchedCategories(null, "https://edition.cnn.com/sport");
        assertTrue(responseDto.getData().size() > 0);
    }

    @Test
    @DisplayName("Test for one subset given of categories")
    void findMatchedCategoriesGivenSubsetShouldReturnMinimumOneCategory() {
        var responseDto = categoryService.findMatchedCategories(List.of("Football", "Basketball"), "https://edition.cnn.com/sport");
        assertTrue(responseDto.getData().size() > 0);
    }

    @Test
    @DisplayName("Test for given invalid url")
    void findMatchedCategoriesGivenInvalidUrlShouldThrowException() {
        assertThrows(InvalidUrlException.class, () -> {
            categoryService.findMatchedCategories(null, "https://edition.cnn.com/invalid");
        });
    }
}