package az.techlab.helper;

import az.techlab.init.ModelInit;
import az.techlab.model.Category;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Category Matching Helper Test")
class CategoryHelperTest {

    private final CategoryHelper categoryHelper = new CategoryHelper();

    @DisplayName("Parse CNN's sport section and find matched categories")
    @Test
    void findMatchedCategoriesGivenCnnShouldReturnMinimumOneCategory() throws IOException {
        List<Category> categories = ModelInit.initCategories();
        String url = "https://edition.cnn.com/sport";
        Map<String, Integer> matchedCategories = categoryHelper.findMatchedCategories(categories, url);
        assertTrue(matchedCategories.size() >= 1);
    }

    @DisplayName("Parse CNN's sport section, with invalid categories")
    @Test
    void findMatchedCategoriesGivenCnnAndInvalidCategoryShouldReturnZeroSize() throws IOException {
        List<Category> categories = ModelInit.initCategories().stream()
                .filter(category -> category.getName().equals("StarWars"))
                .collect(java.util.stream.Collectors.toList());
        String url = "https://edition.cnn.com/sport";
        Map<String, Integer> matchedCategories = categoryHelper.findMatchedCategories(categories, url);
        assertEquals(0, matchedCategories.size());
    }

    @DisplayName("Parse CNN's sport section, with invalid categories")
    @Test
    void findMatchedCategories2() throws IOException {
        List<Category> categories = ModelInit.initCategories().stream()
                .filter(category -> category.getName().equals("StarWars"))
                .collect(java.util.stream.Collectors.toList());
        String url = "https://edition.cnn.com/sport";
        Map<String, Integer> matchedCategories = categoryHelper.findMatchedCategories(categories, url);
        assertEquals(0, matchedCategories.size());
    }

    @DisplayName("Test for null subset given of categories")
    @Test
    void filteredCategoriesGivenNullShouldReturnSizeOf3() {
        List<Category> categories = categoryHelper.filteredCategories(null);
        assertEquals(3, categories.size());
    }

    @DisplayName("Test for given empty subset of categories")
    @Test
    void filteredCategoriesGivenEmptyShouldReturnSizeOf3() {
        List<Category> categories = categoryHelper.filteredCategories(List.of());
        assertEquals(3, categories.size());
    }

    @DisplayName("Test for given invalid subset of categories")
    @Test
    void filteredCategoriesGivenInvalidShouldReturnSizeOf0() {
        List<Category> categories = categoryHelper.filteredCategories(List.of("invalid"));
        assertEquals(0, categories.size());
    }

    @DisplayName("Test for given 1 category subset")
    @Test
    void filteredCategoriesGiven1ShouldReturnSizeOf1() {
        List<Category> categories = categoryHelper.filteredCategories(List.of("Football"));
        assertEquals(1, categories.size());
    }

    @DisplayName("Test for given 2 categories subset")
    @Test
    void filteredCategoriesGiven2ShouldReturnSizeOf2() {
        List<Category> categories = categoryHelper.filteredCategories(List.of("Football", "Basketball"));
        assertEquals(2, categories.size());
    }
}