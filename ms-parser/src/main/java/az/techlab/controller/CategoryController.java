package az.techlab.controller;

import az.techlab.dto.CategoryResponseDto;
import az.techlab.dto.UrlRequestDto;
import az.techlab.exception.InvalidUrlException;
import az.techlab.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/category")
@RequiredArgsConstructor
@Slf4j
@Tag(name = "category-finder", description = "The Category Finder API")
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CategoryController {
    CategoryService service;

    @PostMapping("/find")
    @Operation(summary = "Find given URL category",
            description = "Find Given URL Category (URL and categories (optional) are given in request body)")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation",
                    content = @Content(schema = @Schema(implementation = CategoryResponseDto.class))),
            @ApiResponse(responseCode = "400", description = "Provided url is invalid",
                    content = @Content(schema = @Schema(implementation = InvalidUrlException.class)))})
    public ResponseEntity<CategoryResponseDto> findMatchedCategories(@RequestBody @Valid UrlRequestDto urlRequestDto) {
        log.info("Find matched categories from given ones {} for url: {}", urlRequestDto.getCategories(), urlRequestDto.getUrl());
        return ResponseEntity.ok(service.findMatchedCategories(urlRequestDto.getCategories(), urlRequestDto.getUrl()));
    }
}
