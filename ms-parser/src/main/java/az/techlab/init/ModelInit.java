package az.techlab.init;

import az.techlab.model.Category;
import az.techlab.model.Keyword;

import java.util.List;

public class ModelInit {

    public static List<Category> initCategories() {
        return List.of(
                Category.builder()
                        .name("StarWars")
                        .keywords(List.of(
                                new Keyword("star war"),
                                new Keyword("star wars"),
                                new Keyword("r2d2"),
                                new Keyword("darth vader"),
                                new Keyword("may the force be with you")
                        ))
                        .build(),
                Category.builder()
                        .name("Basketball")
                        .keywords(List.of(
                                new Keyword("basketball"),
                                new Keyword("nba"),
                                new Keyword("ncaa"),
                                new Keyword("lebron james"),
                                new Keyword("john stokton"),
                                new Keyword("anthony davis")
                        ))
                        .build(),
                Category.builder()
                        .name("Football")
                        .keywords(List.of(
                                new Keyword("football"),
                                new Keyword("Messi"),
                                new Keyword("Ronaldo"),
                                new Keyword("Rooney"),
                                new Keyword("Zidane"),
                                new Keyword("Kane")
                        ))
                        .build()
        );
    }
}
