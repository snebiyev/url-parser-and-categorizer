package az.techlab.exception;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static az.techlab.constants.HttpResponseConstants.*;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler extends DefaultErrorAttributes {

    private static final String ARGUMENT_VALIDATION_FAILED = "Argument validation failed";

    @ExceptionHandler(InvalidUrlException.class)
    public final ResponseEntity<Map<String, Object>> handle(IllegalArgumentException ex,
                                                            WebRequest request) {
        log.trace("Illegal argument (invalid url) {}", ex.getMessage());
        return ofType(request, ARGUMENT_VALIDATION_FAILED, List.of("Invalid url"));
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public final ResponseEntity<Map<String, Object>> handle(MethodArgumentTypeMismatchException ex,
                                                            WebRequest request) {
        log.trace("Method arguments are not valid {}", ex.getMessage());
        return ofType(request, ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public final ResponseEntity<Map<String, Object>> handle(MethodArgumentNotValidException ex,
                                                            WebRequest request) {
        log.trace("Endpoint arguments are not valid {}", ex.getMessage());
        return ofType(request, ex.getMessage());
    }

    @ExceptionHandler(MismatchedInputException.class)
    public final ResponseEntity<Map<String, Object>> handle(MismatchedInputException ex,
                                                            WebRequest request) {
        log.trace("Mismatched input {}", ex.getMessage());
        return ofType(request, ex.getMessage());
    }

    protected ResponseEntity<Map<String, Object>> ofType(WebRequest request, String message) {
        return ofType(request, message, Collections.EMPTY_LIST);
    }

    @SuppressWarnings("rawtypes")
    private ResponseEntity<Map<String, Object>> ofType(WebRequest request, String message,
                                                       List validationErrors) {
        Map<String, Object> attributes = getErrorAttributes(request, ErrorAttributeOptions.defaults());
        attributes.put(STATUS, HttpStatus.BAD_REQUEST.value());
        attributes.put(ERROR, HttpStatus.BAD_REQUEST.getReasonPhrase());
        attributes.put(MESSAGE, message);
        attributes.put(ERRORS, validationErrors);
        attributes.put(PATH, ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(attributes, HttpStatus.BAD_REQUEST);
    }

}
