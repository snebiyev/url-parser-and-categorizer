package az.techlab.helper;

import az.techlab.factory.ParserFactory;
import az.techlab.init.ModelInit;
import az.techlab.model.Category;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CategoryHelper {
    private static final Parser parser = ParserFactory.getParser("URL");

    /**
     * Finds matched categories for given URL and returns a map of matched categories and their rank.
     *
     * @param searchInCategories List of categories to search in (if null or empty, then all categories will be searched)
     * @param givenUrl           URL to search in
     * @return Map of matched categories and their rank ( if category is matched more than once, then rank will be increased)
     * @throws IOException if URL could not be parsed
     */
    public Map<String, Integer> findMatchedCategories(List<Category> searchInCategories, String givenUrl) throws IOException {
        Map<String, Integer> foundCategories = new HashMap<>();
        String finalResult = parser.parseText(givenUrl);
        searchInCategories.forEach(category -> {
            category.getKeywords().forEach(keyword -> {
                if (finalResult.toLowerCase().matches("(.*\\b)" + keyword.getPhrase().toLowerCase() + "(\\b.*)")) {
                    foundCategories.merge(category.getName(), 1, Integer::sum);
                }
            });
        });

        return foundCategories;
    }

    public List<Category> filteredCategories(List<String> subSetCategories) {
        List<Category> categories;
        // If categories are not provided, use default categories
        if (subSetCategories == null || subSetCategories.isEmpty()) {
            categories = ModelInit.initCategories();
        } else {
            categories = ModelInit.initCategories().stream()
                    .filter(category -> subSetCategories.contains(category.getName()))
                    .collect(java.util.stream.Collectors.toList());
        }
        return categories;
    }
}
