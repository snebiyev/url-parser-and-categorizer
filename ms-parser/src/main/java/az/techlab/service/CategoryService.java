package az.techlab.service;

import az.techlab.dto.CategoryResponseDto;
import az.techlab.exception.InvalidUrlException;
import az.techlab.helper.CategoryHelper;
import az.techlab.model.Category;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CategoryService {
    CategoryHelper categoryHelper;

    /**
     * Finds matched categories for given URL and returns a map of matched categories and their rank.
     *
     * @param subSetCategories List of categories to search in (if null or empty, then all categories will be searched)
     * @param url              URL to search in
     * @return CategoryResponseDto with matched categories (data field) and their rank ( if category is matched more than once, then rank will be increased)
     * @throws InvalidUrlException if URL could not be parsed
     */
    public CategoryResponseDto findMatchedCategories(List<String> subSetCategories, String url) {
        List<Category> categories = categoryHelper.filteredCategories(subSetCategories);
        try {
            Map<String, Integer> matchedCategories = categoryHelper.findMatchedCategories(categories, url);
            return CategoryResponseDto.builder()
                    .status(200)
                    .message("OK")
                    .data(matchedCategories)
                    .build();
        } catch (IOException e) {
//            return CategoryResponseDto.builder()
//                    .status(400)
//                    .message("Bad Request")
//                    .build();
//            return categoryResponseDto;
            throw new InvalidUrlException("Invalid URL: " + url);
        }
    }


}
