package az.techlab;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class MsParserApplication {
    public static void main(String[] args) {
        SpringApplication.run(MsParserApplication.class, args);
    }

}
