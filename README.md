# URL PARSER AND CATEGORIZATION API

## About

This is an API for parsing URL and categorization of given site based on predefined categories:

1. Basketball
2. Football
3. StarWars

## How to run

**Prerequisites**

- [`Java 11+`](https://www.oracle.com/java/technologies/downloads/#java11)
- [`Docker`](https://www.docker.com/)
- [`Docker-Compose`](https://docs.docker.com/compose/install/)

* It is possible to run ms-parser as standalone application. Just execute MsParserApplication's main method.
* It is also possible to run this service as Docker container using `docker-compose.yml` file.
    * To run this service as Docker container, you need to have Docker installed.
    * Before running docker-compose, you need to:
      * Open file `gradle.properties` and modify following lines:
        * dockerRepoUrl
        * dockerHubEmail
        * dockerHubPassword
        * dockerHubUsername
      * Run command `./gradlew make`
    * Go to project root directory and run `docker-compose up`

## How to use

* You can test an API with Postman.
    * You can find the Postman collection in `postman` folder.
        * There are two files (import them into Postman):
            * `Postman Environment Variables`
            * `Postman Collection file`
* Or you can test it via the Swagger UI.
    * Run service
    * Open the browser and go to `http://localhost:8080/swagger.html`
    * Examples of request body
      * ```json
        {
         "url": "https://edition.cnn.com/sport",
         "categories":[
          "Football"
           ]
        }
        ```
      * ```json
         {
         "url": "https://edition.cnn.com/sport",
         "categories":[
         "Football",
         "Basketball"
            ]
         }
         ```
      * ```json
        {
        "url": "https://edition.cnn.com/sport"
        }
        ```
