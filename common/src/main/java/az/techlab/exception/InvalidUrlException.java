package az.techlab.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InvalidUrlException extends IllegalArgumentException {
    private static final long serialVersionUID = 5530733754303506170L;

    public InvalidUrlException(String message) {
        super(message);
        log.info("Invalid url: {}", message);
    }
}
