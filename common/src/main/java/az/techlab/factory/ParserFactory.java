package az.techlab.factory;

import az.techlab.helper.Parser;
import az.techlab.helper.UrlParser;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ParserFactory {
    public static Parser getParser(String parserType) {
        if (parserType == null) {
            log.error("Parser type is null");
            throw new IllegalArgumentException("Parser type is null");
        }
        if (parserType.equalsIgnoreCase("URL")) {
            return new UrlParser();
        } else if (parserType.equalsIgnoreCase("TEXT")) {
            log.error("Parser type is not supported: {}", parserType);
            throw new IllegalArgumentException("Text parser is not implemented yet");
        }
        log.error("Parser is not implemented yet: {}", parserType);
        throw new IllegalArgumentException("Parser is not implemented yet");
    }
}
