package az.techlab.helper;

import java.io.IOException;

public interface Parser {
    String parseText(String url) throws IOException;
}
