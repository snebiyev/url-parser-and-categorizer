package az.techlab.helper;

import az.techlab.exception.InvalidUrlException;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

@Slf4j
public class UrlParser implements Parser {
    Validator validator = new UrlValidator();

    @Override
    public String parseText(String url) throws IOException {
        if (!validator.validateUrl(url)) {
            log.info("Invalid url: {}", url);
            throw new InvalidUrlException("Invalid URL");
        }
        Document doc = Jsoup.connect(url).get();
        return doc.getAllElements().text();
    }

}
