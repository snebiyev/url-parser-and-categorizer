package az.techlab.factory;

import az.techlab.helper.UrlParser;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Test for ParserFactory class")
class ParserFactoryTest {

    @Test
    @DisplayName("Test for invalid parser type")
    void getParserGivenInvalidParserShouldThrowException() {
        assertThrows(IllegalArgumentException.class, () -> ParserFactory.getParser("invalid"));
    }

    @Test
    @DisplayName("Test for throwing exception for TEXT parser")
    void getParserGivenTextShouldThrowException() {
        assertThrows(IllegalArgumentException.class, () -> ParserFactory.getParser("text"));
    }

    @Test
    @DisplayName("Test for null parser type")
    void getParserGivenNullShouldThrowException() {
        assertThrows(IllegalArgumentException.class, () -> ParserFactory.getParser(null));
    }

    @Test
    @DisplayName("Test for valid parser type")
    void getParserGivenURLShouldReturnInstanceOfUrlParser() {
        assertTrue(ParserFactory.getParser("URL") instanceof UrlParser);
    }
}
